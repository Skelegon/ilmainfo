package com.example.stevesulev.ilmainf;

import java.util.ArrayList;
import java.util.List;

public class Night {
    private String phenomenon;
    private Integer tempmin;
    private Integer tempmax;
    private String text;
    private List<Place> place;
    private List<Integer> windSpeeds = new ArrayList<>();

    public List<Integer> getWindSpeeds() {
        return windSpeeds;
    }

    public void setWindSpeeds(List<Integer> windSpeeds) {
        this.windSpeeds = windSpeeds;
    }

    public String getPhenomenon() {
        return phenomenon;
    }

    public void setPhenomenon(String phenomenon) {
        this.phenomenon = phenomenon;
    }

    public Integer getTempmin() {
        return tempmin;
    }

    public void setTempmin(Integer tempmin) {
        this.tempmin = tempmin;
    }

    public Integer getTempmax() {
        return tempmax;
    }

    public void setTempmax(Integer tempmax) {
        this.tempmax = tempmax;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Place> getPlace() {
        return place;
    }

    public void setPlace(List<Place> place) {
        this.place = place;
    }
}
