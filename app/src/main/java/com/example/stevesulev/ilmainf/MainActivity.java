package com.example.stevesulev.ilmainf;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends AppCompatActivity {

    private List<Weather> weatherInfo = new ArrayList<>();
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ReadRSS rss = new ReadRSS();
        rss.execute();
    }

    public class ReadRSS extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(MainActivity.this, "", "Kogun andmeid, palun oota.", false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            populateUI();
        }

        @Override
        protected Void doInBackground(Void... params) {
            processXml(getData());
            return null;
        }

        public Document getData(){
            try {
                URL url = new URL("http://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                InputStream inputStream = connection.getInputStream();
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = builderFactory.newDocumentBuilder();
                Document xmlDoc = builder.parse(inputStream);
                return xmlDoc;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private void processXml(Document data) {
            if (data!=null){
                Element root = data.getDocumentElement();
                NodeList forecasts = root.getChildNodes();
                for (int i = 0; i < forecasts.getLength(); i++) {
                    Weather weather = new Weather();
                    Wind wind = new Wind();
                    Node currentChild = forecasts.item(i);
                    if (currentChild.getNodeName().equalsIgnoreCase("forecast")){
                        String kpv = currentChild.getAttributes().item(0).getTextContent();
                        NodeList items = currentChild.getChildNodes();
                        for (int j = 0; j < items.getLength(); j++) {
                            Node curChild = items.item(j);
                            if (curChild.getNodeName().equalsIgnoreCase("night")){
                                Night n = new Night();
                                weather.setDate(kpv);
                                NodeList itemChilds = curChild.getChildNodes();
                                for (int k = 0; k < itemChilds.getLength(); k++) {
                                    Node current = itemChilds.item(k);
                                    if (current.getNodeName().equalsIgnoreCase("tempmin")) {
                                        n.setTempmin(Integer.parseInt(current.getTextContent()));
                                    } else if (current.getNodeName().equalsIgnoreCase("tempmax")) {
                                        n.setTempmax(Integer.parseInt(current.getTextContent()));
                                    } else if (current.getNodeName().equalsIgnoreCase("text")) {
                                        n.setText(current.getTextContent());
                                    } else if (current.getNodeName().equalsIgnoreCase("wind")){
                                        NodeList windChildren = current.getChildNodes();
                                        for (int l = 0; l < windChildren.getLength(); l++) {
                                            Node currentWind = windChildren.item(l);
                                            if (currentWind.getNodeName().equalsIgnoreCase("speedmin") || currentWind.getNodeName().equalsIgnoreCase("speedmax")){
                                                n.getWindSpeeds().add(Integer.parseInt(currentWind.getTextContent()));
                                            }
                                        }
                                    } else if (current.getNodeName().equalsIgnoreCase("phenomenon")) {
                                        n.setPhenomenon(current.getTextContent());
                                    }
                                }
                                weather.setNight(n);
                            } else if (curChild.getNodeName().equalsIgnoreCase("day")){
                                Day d = new Day();
                                NodeList itemChilds = curChild.getChildNodes();
                                for (int k = 0; k < itemChilds.getLength(); k++) {
                                    Node current = itemChilds.item(k);
                                    if (current.getNodeName().equalsIgnoreCase("tempmin")) {
                                        d.setTempmin(Integer.parseInt(current.getTextContent()));
                                    } else if (current.getNodeName().equalsIgnoreCase("tempmax")) {
                                        d.setTempmax(Integer.parseInt(current.getTextContent()));
                                    } else if (current.getNodeName().equalsIgnoreCase("text")) {
                                        d.setText(current.getTextContent());
                                    } else if (current.getNodeName().equalsIgnoreCase("wind")){
                                        NodeList windChildren = current.getChildNodes();
                                        for (int l = 0; l < windChildren.getLength(); l++) {
                                            Node currentWind = windChildren.item(l);
                                            if (currentWind.getNodeName().equalsIgnoreCase("speedmin") || currentWind.getNodeName().equalsIgnoreCase("speedmax")){
                                                d.getWindSpeeds().add(Integer.parseInt(currentWind.getTextContent()));
                                            }
                                        }
                                     } else if (current.getNodeName().equalsIgnoreCase("phenomenon")) {
                                        d.setPhenomenon(current.getTextContent());
                                    }
                                }
                                weather.setDay(d);
                            }
                        }
                        weatherInfo.add(weather);
                    }
                }
            }
        }
    }

    private String getWindSpeed(List<Integer> windSpeeds) {
        Collections.sort(windSpeeds);
        if(windSpeeds.size() > 0){
            return "Tuule kiirus: " + windSpeeds.get(0) + "..." + windSpeeds.get(windSpeeds.size()-1) + " m/s";
        }
        return "";
    }

    private void populateUI() {
        //Add dates to spinner and assign change listener.
        final Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        List<String> dates = new ArrayList<>();
        for(Weather i : weatherInfo){
            dates.add(i.getDate());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, dates);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    for(Weather ilm : weatherInfo){
                        if(ilm.getDate().equals(spinner.getSelectedItem().toString())){
                            // Day info
                            int tempMinD = ilm.getDay().getTempmin();
                            int tempMaxD = ilm.getDay().getTempmax();
                            String textD = ilm.getDay().getText();
                            String windSpeedsD = getWindSpeed(ilm.getDay().getWindSpeeds());
                            String phenomenonD = Utilities.translatePhenomenon(ilm.getDay().getPhenomenon());
                            String phenomenonPicD = (Utilities.getPhenomenonImage(ilm.getDay().getPhenomenon()));
                            int phenomenonPicDid = getResources().getIdentifier("com.example.stevesulev.ilmainf:drawable/" + phenomenonPicD, null, null);

                            ((TextView)findViewById(R.id.dayTemp)).setText(tempMinD + "..." + tempMaxD + " °C");
                            ((TextView)findViewById(R.id.dayTempString)).setText(Utilities.getWordFromNumber(tempMinD) + " kuni " + Utilities.getWordFromNumber(tempMaxD)+ " kraadi");
                            ((TextView)findViewById(R.id.day)).setText(textD);
                            ((TextView)findViewById(R.id.dayWind)).setText(windSpeedsD);
                            ((TextView)findViewById(R.id.dayPhenomenon)).setText(phenomenonD);
                            ((ImageView) findViewById(R.id.dayPhenomenonPic)).setImageResource(phenomenonPicDid);

                            // Night info
                            int tempMinN = ilm.getNight().getTempmin();
                            int tempMaxN = ilm.getNight().getTempmax();
                            String textN = ilm.getNight().getText();
                            String windSpeedsN = getWindSpeed(ilm.getNight().getWindSpeeds());
                            String phenomenonN = Utilities.translatePhenomenon(ilm.getNight().getPhenomenon());
                            String phenomenonPicN = (Utilities.getPhenomenonImage(ilm.getNight().getPhenomenon()));
                            int phenomenonPicNid = getResources().getIdentifier("com.example.stevesulev.ilmainf:drawable/" + phenomenonPicN, null, null);

                            ((TextView)findViewById(R.id.nightTemp)).setText(tempMinN + "..." + tempMaxN+ " °C");
                            ((TextView)findViewById(R.id.nightTempString)).setText(Utilities.getWordFromNumber(tempMinN) + " kuni " + Utilities.getWordFromNumber(tempMaxN) + " kraadi");
                            ((TextView)findViewById(R.id.night)).setText(textN);
                            ((TextView)findViewById(R.id.nightWind)).setText(windSpeedsN);
                            ((TextView)findViewById(R.id.nightPhenomenon)).setText(phenomenonN);
                            ((ImageView) findViewById(R.id.nightPhenomenonPic)).setImageResource(phenomenonPicNid);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {}
            }
        );
    }
}
