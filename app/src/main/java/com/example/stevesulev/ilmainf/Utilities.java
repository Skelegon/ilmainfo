package com.example.stevesulev.ilmainf;

public class Utilities {

    public static String getWordFromNumber(int i) {
        String s = (i == 0) ? "" : (i < 0) ? "miinus " : "pluss ";
        String[] n = { "null", "üks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "üheksa", "kümme" };
        i = Math.abs(i);
        if(i >= 0 && i <= 10) {
            return s + n[i];
        }  else if(i > 10 && i < 100) {
            int a = i / 10; // First half of the number
            int b = i % 10; // Second half of the number
            return s + ((a == 1) ? n[b] + "teist" : n[a] + "kümmend") + ((b == 0 || a == 1) ? "" : " " + n[b]);
        } else {
            return s + i;
        }
    }

    public static String translatePhenomenon(String phenomenon) {

        String [] english = {"Clear", "Few clouds", "Variable clouds", "Cloudy with clear spells", "Cloudy", "Light snow shower", "Moderate snow shower", "Heavy snow shower", "Light shower", "Moderate shower", "Heavy shower", "Light rain", "Moderate rain", "Heavy rain", "Risk of glaze", "Light sleet", "Moderate sleet", "Light snowfall", "Moderate snowfall", "Heavy snowfall", "Snowstorm", "Drifting snow", "Hail", "Mist", "Fog", "Thunder", "Thunderstorm"};
        String [] estonian = {"Selge", "Vähene pilvisus", "Vahelduv pilvisus", "Pilves selgimistega", "Pilves", "Nõrk hooglumi", "Mõõdukas hooglumi", "Tugev hooglumi", "Nõrk hoogvihm", "Mõõdukas hoogvihm", "Tugev hoogvihm", "Nõrk vihm", "Mõõdukas vihm", "Tugev vihm", "Jäiteoht", "Nõrk lörtsisadu", "Mõõdukas lörtsisadu", "Nõrk lumesadu", "Mõõdukas lumesadu", "Tugev lumesadu", "Üldtuisk", "Pinnatuisk", "Rahe", "Uduvine", "Udu", "Äike", "Äikesevihm"};

        int index = -1;
        for (int i=0;i<english.length;i++) {
            if (english[i].equals(phenomenon)) {
                index = i;
                break;
            }
        }
        if (index > -1) return estonian[index];
        else return null;
    }

    public static String getPhenomenonImage(String phenomenon) {

        String [] phenomenonArray = {"Clear", "Few clouds", "Variable clouds", "Cloudy with clear spells", "Cloudy", "Light snow shower", "Moderate snow shower", "Heavy snow shower", "Light shower", "Moderate shower", "Heavy shower", "Light rain", "Moderate rain", "Heavy rain", "Risk of glaze", "Light sleet", "Moderate sleet", "Light snowfall", "Moderate snowfall", "Heavy snowfall", "Snowstorm", "Drifting snow", "Hail", "Mist", "Fog", "Thunder", "Thunderstorm"};
        String [] pictureName ={"sunny", "mostlycloudy", "mostlycloudy", "mostlycloudy", "cloudy", "snow", "snow", "snow", "slightdrizzle", "slightdrizzle", "slightdrizzle", "drizzle", "drizzle", "drizzle", "snow", "snow", "snow", "snow", "snow", "snow", "snow", "snow", "snow", "haze", "haze", "thunderstorms", "thunderstorms"};

        int index = -1;
        for (int i=0;i<phenomenonArray.length;i++) {
            if (phenomenonArray[i].equals(phenomenon)) {
                index = i;
                break;
            }
        }
        if (index > -1) return pictureName[index];
        else return null;

    }
}
